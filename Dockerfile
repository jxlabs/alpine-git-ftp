FROM alpine:edge

LABEL maintainer="jx"

ENTRYPOINT ["/bin/sh", "-c"]

RUN apk --update add bash git curl && \
    wget -P /usr/local/bin https://raw.githubusercontent.com/git-ftp/git-ftp/master/git-ftp && \
    chmod +x /usr/local/bin/git-ftp && \
    rm -rf /tmp/ && \
    rm -rf /var/cache/apk/